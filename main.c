#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct engine {
	char name[50];
	char url[100];
};

struct engine engine;

int is_valid_engine(struct engine ens[], char name[], int len, struct engine *ch_ptr){
	int eq, lp;
	for(lp=0; lp<len; lp++){
		eq = strcmp(ens[lp].name, name);
		if(eq==0){
			*ch_ptr = ens[lp];
			return 1;
		} 
	}
	return 0;
}

char *get_new_str_ptr(int argc, char **argv, int url_len){
	int ret = 0;
	int i;
	for(i=2; i<argc; i++){
		ret = (ret + strlen(argv[i]));
		if(i!= (argc-1)){
			ret = ret+1;
		}
	}
	
	ret = ret+url_len;
	
	/* Accomodate 'xdg-open ' */
	ret = ret+9;
	/* This bad boy is for \0 */
	ret = ret+1;
	return (char*) calloc(ret, sizeof(char));
}

void create_cmd_with_url(char new_str[], char **argv, int argc, char url[]){
	int i, url_len;
	strcpy(new_str, "xdg-open ");
	
	/* Gotta get rid of that there \n character */
	url_len = strlen(url);
	strncat(new_str, url, (url_len - 1));

	for(i=2;  i<argc; i++){
		strcat(new_str, argv[i]);
		if(i!= (argc-1)){
			strcat(new_str, "+");
		}
	}
	strcat(new_str, "\0");
}

// ignore this function;
struct engine *parse_config(int *engine_len){
	struct engine *engines;
	char config_path[100];
	char ln[128];
	char *spl;
	FILE *ftr;
	int iter = 0;
	
	strcpy(config_path, getenv("HOME"));
	strcat(config_path, "/.config/csurf/config");
	if ((ftr = fopen(config_path, "r")) == NULL ){
		printf("Error reading config file\n");
		exit(1);
	}

	/* Count number of lines */
	while(fgets(ln, sizeof ln, ftr) != NULL ){
		iter = iter+1;
	}

	engines = (struct engine*) calloc(iter *  sizeof engine, sizeof engine);
	*engine_len = iter;
	/*Go to top of file */
	rewind(ftr);
	
	iter = 0;

	/* Iterate over lines and add them to struct */
	while(fgets(ln,  sizeof ln, ftr)){
		spl = strtok(ln, "|");
		strcpy(engines[iter].name, spl);

		spl = strtok(NULL, "|");
		strcpy(engines[iter].url, spl);

		iter = iter+1;	
	}
	fclose(ftr);
	return engines;
}

int main(int argc, char **argv){
	struct engine *engines, chosen_engine, *chosen_en_ptr;
	chosen_en_ptr = &chosen_engine;

	int i, engine_len, *engine_len_ptr, valid;
	char *new_str;
	engine_len = 0;
	engine_len_ptr = &engine_len;

	if(argc <= 2) {
		printf("insufficient parameters\n");
		return -1;
	}

	/*set_ens(engines);		*/
    engines = parse_config(engine_len_ptr);
	if(!is_valid_engine(engines, argv[1], *engine_len_ptr, chosen_en_ptr)){
		printf("Engine not found. Configure it maybe?\n");
		return -1;
	}
	new_str = get_new_str_ptr(argc, argv, strlen(chosen_en_ptr->url));
	create_cmd_with_url(new_str, argv, argc, chosen_en_ptr->url);
	system(new_str);

	free(new_str);
	free(engines);
	exit(0);
	return 0;
}
