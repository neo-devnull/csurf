all: main.c
	gcc -o csurf main.c
	mkdir $$HOME/.config/csurf -p
	cp config.example $$HOME/.config/csurf/config
clean:
	$(RM) csurf
	rm -rf $$HOME/.config/csurf/
